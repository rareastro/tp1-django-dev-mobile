# Utilisation de l'application

## Installer dépendances (venv) (windows)
1. Cloner l'application
2. Rentrer dans le dossier `tp1-django-dev-mobile`
3. Ouvrir powershell/cmd & lancer `venv` : `py -m venv venv`
4. Activer `venv` dans l'invite de commande : `venv\Scripts\activate`
5. Installer les dépendances : `py -m pip install -r requis.txt`

## Lancer l'application
1. Aller dans le dossier `TP1_DeghmousDanny`
2. Lancer l'application `py manage.py runserver`