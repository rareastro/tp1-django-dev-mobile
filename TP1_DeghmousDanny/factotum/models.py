from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone

# (utilisés dans forms.py)
MAX_TAUX_HORAIRE = 999999
ROLES_UTILISATEUR = (("UTILISATEUR", "UTILISATEUR"), ("PROFESSIONNEL", "PROFESSIONNEL"))
ETATS_SOUMISSION = (("EN_ATTENTE", "EN_ ATTENTE"), ("ACCEPTEE", "ACCEPTEE"),
                    ("TERMINEE", "TERMINEE"), ("ANNULEE", "ANNULEE"))


class User(AbstractUser):
    """
    Modèle utilisateur.
    Un utilisateur peut avoir plusieurs rôles de professionnel.
    """
    role = models.CharField(max_length=60, choices=ROLES_UTILISATEUR)
    adresse = models.CharField(max_length=255, null=False, blank=False)
    code_postal = models.CharField(max_length=50, null=False, blank=False)
    tel = models.CharField(max_length=50, null=False, blank=False)
    description = models.TextField(max_length=60, null=False, blank=False)

    # lien vers l'avatar /media/avatar/<xxx>
    avatar = models.ImageField(null=False, blank=False, upload_to="avatar/", default='avatar/default_avatar.png')

    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.username})"


class Service(models.Model):
    """
    Modèle service
    """
    nom = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return f"(Service) {self.nom}"


class ProfessionnelService(models.Model):
    """
    Modèle ProfessionnelService.
    Un professionnel est un utilisateur. Un utilisateur peut avoir plusieurs rôles de
    professionnel, mais un professionnel est relié à seulement à un utilisateur

    Un professionnel peut proposer un seul service. Un service peut être
    proposé par plusieurs professionnels.
    """
    taux_horaire = models.FloatField(null=False, blank=False, validators=[
        MaxValueValidator(MAX_TAUX_HORAIRE),
        MinValueValidator(0)])

    # Relations
    utilisateur = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    service = models.ForeignKey(Service, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f"{self.utilisateur} {self.service.nom}"


class Soumission(models.Model):
    """
    Modèle de soumission.
    Un professionnel peut avoir plusieurs soumissions, mais une
    soumission est reliée à un seul professionnel.

    Un utilisateur peut avoir plusieurs soumissions, mais une
    soumission est reliée à un seul utilisateur.
    """
    date_planification = models.DateField(null=False, blank=False, auto_now_add=True)
    date_terminee = models.DateField(null=False, blank=False, default=timezone.now)
    description = models.TextField(max_length=60, null=False, blank=False)
    etat = models.CharField(max_length=60, choices=ETATS_SOUMISSION, null=False, blank=False)
    note = models.IntegerField(validators=[
        MaxValueValidator(5),
        MinValueValidator(0)
    ], null=True, blank=True)

    # Relations
    utilisateur = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    professionnel = models.ForeignKey(ProfessionnelService, on_delete=models.CASCADE, null=False, blank=False)

    def __str__(self):
        return f"(Soumission) Pour {self.utilisateur} - Par {self.professionnel} - {self.etat}"
