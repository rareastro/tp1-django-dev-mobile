from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from . import views

urlpatterns = [
    # Page d'accueil
    path("", views.factotum_accueil, name="factotum_accueil"),

    # Professionnel
    path("professionnel/<int:professionnel_id>/",
         views.factotum_professionnel_details, name="factotum_professionnel_details"),

    # Soumission
    path("soumission/<int:professionnel_id>/new/", views.factotum_soumission, name="factotum_soumission"),
    path("soumission/<int:id_soumission>/noter/", views.factotum_soumission_noter, name='factotum_soumission_noter'),
    path("soumission/<int:id_soumission>/accepter/", views.factotum_soumission_accepter,
         name="factotum_soumission_accepter"),
    path("soumission/<int:id_soumission>/terminer/", views.factotum_soumission_terminer,
         name="factotum_soumission_terminer"),
    path("soumission/<int:id_soumission>/annuler/", views.factotum_soumission_annuler,
         name='factotum_soumission_annuler'),

    # Comptes
    path("accounts/signup/", views.factotum_creation_compte, name="factotum_creation_compte"),
    path("accounts/login/", views.factotum_connection, name="factotum_connection"),
    path('accounts/logout/', views.factotum_deconnexion, name='factotum_deconnexion'),
    path("profil/", views.factotum_utilisateur_details, name="factotum_utilisateur_details"),
    path("professionnel/new/", views.factotum_professionnel_nouveau,
         name="factotum_professionnel_nouveau"),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
