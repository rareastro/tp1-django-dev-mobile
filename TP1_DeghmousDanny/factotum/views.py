import datetime
import json
from urllib.parse import quote

import requests
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from requests import RequestException

from TP1_DeghmousDanny import settings
from .forms import RechercheProfessionnelForm, CreationCompteForm, ConnectionForm, SoumissionForm, \
    NouveauProfessionnelForm
from .models import ProfessionnelService, Soumission, ETATS_SOUMISSION, ROLES_UTILISATEUR, Service

URL_VUE_CONNECTION = "factotum_connection"


def factotum_accueil(request):
    """
    Afficher la page d'accueil
    """
    # objets qui va contenir les distances

    if request.method == "POST":
        distances = []
        form = RechercheProfessionnelForm(request.POST)

        # Afficher erreurs
        if not form.is_valid():
            return render(request, 'factotum/factotum_accueil.html', {"form": form})

        # Service/code postal utilisateur
        service_utilisateur = form.cleaned_data["liste_services"]
        code_postal_utilisateur = form.cleaned_data["code_postal"]

        # Liste des professionnels proposant ce service
        professionnels = ProfessionnelService.objects.filter(service__nom=service_utilisateur)

        # Vérifier si des professionnels offrent le service
        if len(professionnels) <= 0:
            messages.add_message(request, messages.ERROR, "Aucun professionnel n'offre ce service.")
            return render(request, 'factotum/factotum_accueil.html', {"form": form})

        zip_api_key = settings.ZIP_API_KEY

        # Calculer distances entre code postal de l'utilisateur et des professionnels
        # & Calculer les notes des professionnels
        for professionnel in professionnels:
            code_postal_professionnel = professionnel.utilisateur.code_postal

            # Calculer la note du professionnel
            soumissions = Soumission.objects.filter(professionnel__id=professionnel.id)
            note = calculer_note(soumissions)

            # Requête http
            [err_api, reponse] = requete_api(zip_api_key, code_postal_utilisateur, code_postal_professionnel)
            if len(err_api):
                messages.add_message(request, messages.ERROR, err_api)
                return render(request, 'factotum/factotum_accueil.html', {"form": form})

            # Récupérer la distance
            # distance = int(reponse["distance"])
            distances.append({"professionnel": professionnel, "distance": round(3), "note": note})

        # Afficher les professionnels en ordre croissant de distance
        distances = sorted(distances, key=lambda d: d['distance'])

        return render(request, 'factotum/factotum_accueil.html',
                      {"form": form, "distances": distances, "service_utilisateur": service_utilisateur})

    # Afficher formulaire recherche professionnel
    form = RechercheProfessionnelForm()
    return render(request, 'factotum/factotum_accueil.html', {"form": form})


# =================== GESTION COMPTES ===================
def factotum_connection(request):
    """
    Connection
    """
    if request.user.is_authenticated:
        return redirect('factotum_accueil')

    if request.method == "POST":
        form = ConnectionForm(request.POST)

        # Validation formulaire
        if form.is_valid():
            # Authentifier utilisateur
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = authenticate(request, username=username, password=password)
            login(request, user)

            # Redirection
            return redirect("factotum_accueil")

        return render(request, "factotum/factotum_connection.html", {"form": form})

    form = ConnectionForm()
    return render(request, "factotum/factotum_connection.html", {"form": form})


def factotum_deconnexion(request):
    """
    Url de déconnexion
    Peut importe le type de requête (get, post, etc.)
    """
    logout(request)
    return redirect('factotum_accueil')


def factotum_creation_compte(request):
    """
    Connection au compte
    """
    if request.method == "POST":
        form = CreationCompteForm(request.POST, request.FILES)

        # Valider formulaire
        if not form.is_valid():
            # Afficher erreurs
            return render(request, "factotum/factotum_creation_compte.html", {"form": form})

        est_professionnel = form.cleaned_data['est_professionnel']

        # Sauvegarder utilisateur
        nv_utilisateur = form.save()

        # Ajouter à la table des professionnels
        if est_professionnel:
            # Créer ProfessionnelService
            professionnel = ProfessionnelService(taux_horaire=0)
            professionnel.utilisateur = nv_utilisateur
            professionnel.save()

        return redirect('factotum_connection')

    form = CreationCompteForm()
    return render(request, "factotum/factotum_creation_compte.html", {"form": form})


@login_required(login_url=URL_VUE_CONNECTION)
def factotum_utilisateur_details(request):
    """
    Profil utilisateur
    id_utilisateur : Id de l'utilisateur
    """
    # Trouver utilisateur
    utilisateur = request.user

    # Trouver soumissions créées par l'utilisateur
    soumissions_utilisateur = Soumission.objects.filter(utilisateur__id=utilisateur.id)

    # Trouver les professionnels associés à cet utilisateur
    profesionnels_services = ProfessionnelService.objects.filter(utilisateur__id=utilisateur.id)

    # Trouver demandes de soumission (professionnel seulement)
    soumissions_des_professionnels = []
    if profesionnels_services:
        for professionnel in profesionnels_services:
            soum = Soumission.objects.filter(professionnel__id=professionnel.id)
            soumissions_des_professionnels.append(soum)

    return render(request, "factotum/factotum_utilisateur_details.html",
                  {"utilisateur": utilisateur,
                   "soumissions_utilisateur": soumissions_utilisateur,
                   "soumissions_des_professionnels": soumissions_des_professionnels})


@login_required(login_url=URL_VUE_CONNECTION)
def factotum_professionnel_details(request, professionnel_id):
    """
    Afficher le profil d'un professionnel.
    professionnel_id: Id professionnel
    """
    # Chercher professionnel
    professionnel = get_object_or_404(ProfessionnelService, id=professionnel_id)

    return render(request, 'factotum/factotum_professionnel_details.html',
                  {"professionnel": professionnel})


@login_required(login_url=URL_VUE_CONNECTION)
def factotum_professionnel_nouveau(request):
    """
    Ajouter des professionnels services au nom de l'utilisateur actuel
    """
    # Récupérer utilisateur actuel
    utilisateur = request.user

    if request.method == "POST":
        form = NouveauProfessionnelForm(request.POST)

        # Valide formulaire
        if not form.is_valid():
            return render(request, "factotum/factotum_professionnel_nouveau.html", {"form": form})

        taux_horaire = float(form.cleaned_data['taux_horaire'])
        nom_service = form.cleaned_data['liste_services']

        # Trouver le service
        service = Service.objects.get(nom=nom_service)

        # Créer le nouveau professionnel service
        nv_professionnel = ProfessionnelService(taux_horaire=taux_horaire)
        nv_professionnel.utilisateur = request.user
        nv_professionnel.service = service
        nv_professionnel.save()

        messages.add_message(request, messages.SUCCESS, "Succés ! Nouveau service offert.")
        return render(request, "factotum/factotum_professionnel_nouveau.html", {"form": form})

    # Vérifier si utilisateur est un professionnel
    if utilisateur.role != ROLES_UTILISATEUR[1][0]:
        # messages.add_message(request, messages.ERROR, "Seulement les professionnels peuvent accéder à cette page")
        return redirect("factotum_utilisateur_details")

    form = NouveauProfessionnelForm()
    return render(request, "factotum/factotum_professionnel_nouveau.html", {"form": form})


# =================== SOUMISSION ===================
@login_required(login_url=URL_VUE_CONNECTION)
def factotum_soumission(request, professionnel_id):
    """
    Création des soumissions
    professionnel_id : Id professionnel
    """
    professionnel = get_object_or_404(ProfessionnelService, id=professionnel_id)

    if request.method == "POST":
        form = SoumissionForm(data=request.POST)

        # Valider formulaire
        if not form.is_valid():
            # Afficher erreurs
            return render(request, 'factotum/factotum_soumission_new.html',
                          {"form": form, "professionnel": professionnel})

        # Trouver utilisateur ayant fait soumission
        utilisateur = request.user

        # Créer soumission
        soumission = Soumission(date_terminee=form.cleaned_data['date_terminee'],
                                description=form.cleaned_data['description'],
                                etat=ETATS_SOUMISSION[0][0])  # État EN_ATTENTE
        soumission.utilisateur = utilisateur
        soumission.professionnel = professionnel
        soumission.save()

        # Message confirmation
        messages.add_message(request, messages.SUCCESS, "Soumission crée")
        return redirect("factotum_utilisateur_details")

    # Chercher professionnel
    form = SoumissionForm()

    # Calculer note du professionnel
    # Calculer la note du professionnel
    soumissions = Soumission.objects.filter(professionnel__id=professionnel.id)
    note = calculer_note(soumissions)

    return render(request, 'factotum/factotum_soumission_new.html',
                  {"form": form, "professionnel": professionnel, "note": note})


@login_required(login_url=URL_VUE_CONNECTION)
def factotum_soumission_noter(request, id_soumission):
    """
    Url utilisé dans 'factotum_utilisateur_details.html' par javascript
    pour changer la note avec des appels asynchrones

    Seulement l'utilisateur peut noter la soumission

    id_soumission : Id de la soumission
    """
    if request.method == "POST":

        try:
            donnes = json.loads(request.body.decode('utf-8'))
            note = donnes['note']
            soumission = get_object_or_404(Soumission, id=id_soumission)

            # Vérifier que seulement l'utilisateur peut noter la soumission
            if request.user.id != soumission.utilisateur.id:
                return JsonResponse({"msg": "Vous ne detenez pas cette soumission"}, status=403)

            # Vérifier que la soumission est terminée pour pouvoir la noter
            if soumission.etat != ETATS_SOUMISSION[2][0]:
                return JsonResponse({"msg": "La soumission doit être terminée pour pouvoir noter"},
                                    status=400)

            # Vérifier que la soumission n'a pas déjà été noté
            if soumission.note:
                return JsonResponse({"msg": "La soumission peut être noté qu'une seule fois"},
                                    status=400)

            # Noter la soumission
            soumission.note = int(note)
            soumission.save()

            return JsonResponse({"msg": "Note changée"}, safe=True, status=200)

        except Exception as e:
            return JsonResponse({"msg": f"Une erreur s'est produite {e}"}, status=500)
    else:
        return HttpResponseBadRequest("")


@login_required(login_url=URL_VUE_CONNECTION)
def factotum_soumission_annuler(request, id_soumission):
    """
        Url utilisé dans 'factotum_utilisateur_details.html' par javascript
        pour annuler une soumission avec des appels asynchrones

        L'utilisateur et le professionnel peuvent annuler la soumission

        id_soumission : Id de la soumission
        """
    if request.method == "POST":

        try:
            soumission = get_object_or_404(Soumission, id=id_soumission)

            # Vérifier que l'utilisateur détient la soumission
            # ou que le l'utilisateur connecté est le professionnel qui fait la soumission
            if request.user.id != soumission.utilisateur.id:
                if request.user.id != soumission.professionnel.utilisateur.id:
                    return JsonResponse({"msg": "Vous ne detenez pas cette soumission"}, status=403)

            # Vérifier si la soumission a déjà été accepté
            if soumission.etat == ETATS_SOUMISSION[1][0]:
                return JsonResponse({"msg": "Vous ne pouvez pas annuler une soumission en cours"}, status=403)

            # Supprimer soumission
            soumission.delete()

            return JsonResponse({"msg": "Soumission supprimée"}, safe=True, status=200)

        except Exception as e:
            return JsonResponse({"msg": f"Une erreur s'est produite {e}"}, status=500)
    else:
        return HttpResponseBadRequest("")


@login_required(login_url=URL_VUE_CONNECTION)
def factotum_soumission_accepter(request, id_soumission):
    """
    Url utilisé dans 'factotum_utilisateur_details.html' par javascript
    pour accepter une soumission avec des appels asynchrones

    Seulement le professionnel peut accepter la soumission


    id_soumission : Id de la soumission
    """
    if request.method == "POST":

        try:
            soumission = get_object_or_404(Soumission, id=id_soumission)

            # Vérifier que seulement le professionnel peut accepter le service
            if request.user.id != soumission.professionnel.utilisateur.id:
                return JsonResponse({"msg": "Seulement le professionnel peut accpeter cette soumission"}, status=403)

            # On peut accepter une soumission que si l'état actuel est EN_ATTENTE
            if soumission.etat != ETATS_SOUMISSION[0][0]:
                return JsonResponse({"msg": "Vous avez déja accepté cette soumission"}, status=403)

            # Accepter la soumission
            soumission.etat = ETATS_SOUMISSION[1][0]
            soumission.save()

            return JsonResponse({"msg": "Soumission acceptée"}, safe=True, status=200)

        except Exception as e:
            return JsonResponse({"msg": f"Une erreur s'est produite {e}"}, status=500)
    else:
        return HttpResponseBadRequest("")


@login_required(login_url=URL_VUE_CONNECTION)
def factotum_soumission_terminer(request, id_soumission):
    """
    Url utilisé dans 'factotum_utilisateur_details.html' par javascript
    pour terminer une soumission avec des appels asynchrones

    Seulement le professionnel peut terminer la soumission

    id_soumission : Id de la soumission
    """
    if request.method == "POST":

        try:
            soumission = get_object_or_404(Soumission, id=id_soumission)

            # Vérifier que seulement le professionnel peut terminer la soumission
            if request.user.id != soumission.professionnel.utilisateur.id:
                return JsonResponse({"msg": "Seulement le professionnel peut terminer cette soumission"}, status=403)

            # Vérifier si la soumisson est déja terminée
            if soumission.etat == ETATS_SOUMISSION[2][0]:
                return JsonResponse({"msg": "la soumisson est déja terminée"}, status=400)

            # Vérifier que la soumisson a été déja accepté
            if soumission.etat != ETATS_SOUMISSION[1][0]:
                return JsonResponse({"msg": "La soumission doit déja avoir été acceptée pour la terminer"}, status=400)

            # Terminer la soumission
            soumission.etat = ETATS_SOUMISSION[2][0]

            # Mettre à jour la date de fin
            soumission.date_terminee = datetime.datetime.today().date()
            soumission.save()

            return JsonResponse({"msg": "Soumission terminée"}, safe=True, status=200)

        except Exception as e:
            return JsonResponse({"msg": f"Une erreur s'est produite {e}"}, status=500)
    else:
        return HttpResponseBadRequest("")


def requete_api(zip_api_key, code_postal_utilisateur, code_postal_professionnel):
    """
    Requête vers l'api
    Args:
        zip_api_key: Clé de l'api
        code_postal_utilisateur : code postal de l'utilisateur
        code_postal_professionnel: : code postal du professionnel

    Returns: retourne une liste [err, reponse]
    """
    url = f"https://www.zipcodeapi.com/rest/v2/CA/{zip_api_key}/" \
          f"distance.json/{quote(code_postal_utilisateur.upper())}/{quote(code_postal_professionnel.upper())}/km"
    err = ""
    reponse = None

    try:
        req = requests.get(url)
        reponse = req.json()

        # Verifier si erreur de l'api
        if "error_msg" in reponse:
            err_code = reponse['error_code']

            match err_code:
                case 404:
                    # Code postal non trouvé
                    err += f"{reponse['error_msg']}"
                case 429:
                    err += f"Atteint limite de l'api (50 requêtes par jours).\n"
                case _:
                    err += f"Veuillez entrez un code postal Canadien valide.\n"

        return [err, reponse]

    except RequestException as e:
        err = f"Une erreur s'est produite lors de la requête à l'api : {e}"
        return [err, reponse]


def calculer_note(soumissions):
    """
    Calculer la note d'un professionnel
    Args:
        soumissions: Liste des soumissions d'un professionnel

    Returns: La note d'un professionnel. Retourne None si aucune soumission n'existe
    """

    if len(soumissions) == 0:
        return None

    note = 0
    nb_soumissions_avec_note = 0  # Une soumission peut ne pas être noté
    for soum in soumissions:
        if soum.note:
            note += soum.note
            nb_soumissions_avec_note += 1

    return round(note / nb_soumissions_avec_note)
