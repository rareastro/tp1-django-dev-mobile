import datetime
import re

from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.forms import ModelForm

from .models import Service, User, Soumission, ROLES_UTILISATEUR, MAX_TAUX_HORAIRE

# Format A1A 1A1
REGEX_CODE_POSTAL = r"^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTVXY] \d[ABCEGHJ-NPRSTVXY]\d$"

# Format : 111-111-1111
REGEX_TEL = "^[0-9]{3}(-)[0-9]{3}(-)[0-9]{4}$"


class RechercheProfessionnelForm(forms.Form):
    """
    Formulaire recherche professionnel
    """
    liste_services = forms.ChoiceField(choices=[], required=True, label="Liste des services")
    code_postal = forms.CharField(min_length=3, max_length=32, required=True, label="Code postal Canadien",
                                  help_text="format : A1A 1A1")

    def __init__(self, *args, **kwargs):
        """
        Passer la liste des services au formulaire
        https://stackoverflow.com/a/53177617
        https://stackoverflow.com/a/71188140
        """
        super(RechercheProfessionnelForm, self).__init__(*args, **kwargs)
        self.fields['liste_services'].choices = []

        # Récupérer la liste des services
        services = Service.objects.all()
        for i, service in enumerate(services):
            self.fields['liste_services'].choices.append((str(service.nom), service.nom))

    def clean_code_postal(self):
        """
        Vérifier que le code postal est du bon format
        """
        code_postal = self.cleaned_data['code_postal']
        recherche = re.search(REGEX_CODE_POSTAL, code_postal, flags=re.MULTILINE | re.IGNORECASE)

        if recherche is None:
            raise ValidationError("Le code postal doit avoir ce format : A1A 1A1")
        return code_postal


class SoumissionForm(ModelForm):
    """
    Formulaire soumission
    """

    class Meta:
        model = Soumission
        fields = ("date_terminee", "description")

        widgets = {
            "date_terminee": forms.DateInput(attrs={"placeholder": str(datetime.datetime.today().date())},
                                             format='%Y-%m-%d')
        }

        labels = {
            "date_terminee": "Pour quelle date ?",
        }

    def clean_date_terminee(self):
        """
        Vérifier que la date de planification est au moins aujourd'hui, ou plus tard
        """
        date_terminee = str(self.cleaned_data["date_terminee"])
        aujourdhui = str(datetime.datetime.today().date())

        if date_terminee < aujourdhui:
            raise ValidationError("La date doit être antérieure ou égale à aujourd'hui")

        return date_terminee


class NouveauProfessionnelForm(forms.Form):
    """
    Formulaire nouveau professionnel
    """
    liste_services = forms.ChoiceField(required=True, label="Service offert")
    taux_horaire = forms.FloatField(required=True, help_text="Minumum de 0", validators=[
        MaxValueValidator(MAX_TAUX_HORAIRE),
        MinValueValidator(0)
    ])

    def __init__(self, *args, **kwargs):
        """
        Afficher la liste des services
        """
        super(NouveauProfessionnelForm, self).__init__(*args, **kwargs)
        self.fields['liste_services'].choices = []

        services = Service.objects.all()
        for i, service in enumerate(services):
            self.fields['liste_services'].choices.append((str(service.nom), service.nom))


class ConnectionForm(forms.Form):
    """
    Formulaire de connection
    """
    username = forms.CharField(required=True, label="Nom d'utilisateur", min_length=0, max_length=150)
    password = forms.CharField(required=True, label="Mot de passe", min_length=0, max_length=150,
                               widget=forms.PasswordInput())

    def clean(self):
        """
        Vérifier que le nom d'utilisateur existe &
        Vérifier que les mots de passes correspondent
        """
        donnees = self.cleaned_data
        if any(self.errors):
            return

        username = self.cleaned_data['username']
        p1 = self.cleaned_data['password']

        try:
            # Utilisateur existe
            utilisateur = User.objects.get(username=username)

            # Verifier mots de passe
            if not utilisateur.check_password(p1):
                raise ValidationError("Erreur du nom d'utlisateur et/ou du mot de passe.")

            return donnees

        except ObjectDoesNotExist:
            # Utilisateur n'existe pas
            raise ValidationError("Erreur du nom d'utlisateur et/ou du mot de passe.")


class CreationCompteForm(ModelForm):
    """
    Formulaire creation compte
    """
    est_professionnel = forms.BooleanField(required=False, label="Voulez-vous être un professionnel ?",
                                           help_text="Un professionnel peut proposer de vendre ses services")
    password_confirm = forms.CharField(label="Confirmer votre mot de passe", widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = (
            'username', 'email', 'password', 'password_confirm', 'last_name', 'first_name', 'adresse', 'code_postal',
            'tel',
            'avatar', "est_professionnel",)

        widgets = {
            "password": forms.PasswordInput(),
        }

        labels = {
            'username': "Nom d'utilisateur",
            'email': "Courriel",
            'password': "Mot de passe",
            "last_name": "Nom",
            "first_name": "Prénom",
            "code_postal": "Code postal Canadien"
        }

        help_texts = {
            'username': "",
            "tel": 'exemple : 418-553-1253'
        }

    def clean_tel(self):
        """
        Vérifier que le numéro contient seulement des nombres
        """
        tel = self.cleaned_data['tel']
        recherche = re.search(REGEX_TEL, tel)

        if recherche is None:
            raise ValidationError("Le numéro doit avoir ce format : xxx-xxx-xxxx")
        return tel

    def clean_username(self):
        """
        Vérifier que le nom d'utilisateur n'exite pas
        """
        username = self.cleaned_data['username']

        try:
            User.objects.get(username=username)
            # Utilisateur existe
            raise ValidationError("Un compte avec ce nom d'utilisateur existe déjà")
        except ObjectDoesNotExist:
            # Utilisateur n'exite pas
            return username

    def clean_password_confirm(self):
        """
        Confirmer que les mots de passes se correspondent
        """
        p1 = self.cleaned_data['password']
        p2 = self.cleaned_data['password_confirm']

        if p1 != p2:
            raise ValidationError("Les deux mots de passes ne correspondent pas")
        return p2

    def save(self, commit=True):
        """
        Hasher le mdp & définir le rôle
        """

        # Mdp
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])

        # Role
        est_professionnel = self.cleaned_data['est_professionnel']
        if est_professionnel:
            print(f'Assignation du role {ROLES_UTILISATEUR[1][0]}')
            user.role = ROLES_UTILISATEUR[1][0]  # Assigner role de professionnel
        else:
            print(f'Assignation du role {ROLES_UTILISATEUR[0][0]}')
            user.role = ROLES_UTILISATEUR[0][0]  # Assigner role d'utilisateur

        if commit:
            user.save()
        return user
