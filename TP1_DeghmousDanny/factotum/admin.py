from django.contrib import admin
from .models import User, Service, ProfessionnelService, Soumission


# User
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'first_name', 'last_name', 'email', 'role')


# Professionnel
@admin.register(ProfessionnelService)
class ProfessionnelServiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_user_id', 'get_user', 'get_service', 'taux_horaire')

    @admin.display(description="USER ID")
    def get_user_id(self, obj):
        return f"{obj.utilisateur.id}"

    @admin.display(description="Utilisateur")
    def get_user(self, obj):
        return f"{obj.utilisateur}"

    @admin.display(description="Service")
    def get_service(self, obj):
        # https://stackoverflow.com/a/18108586
        # services = [service.nom for service in obj.service.all()]
        # return ", ".join(services)
        return f"{obj.service}"


# Service
@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'nom',)


# Soumission
@admin.register(Soumission)
class SoumissionAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'get_user', 'get_professionnel', 'etat', 'note', 'description', 'date_planification', 'date_terminee',)

    @admin.display(description="Utilisateur")
    def get_user(self, obj):
        return obj.utilisateur

    @admin.display(description="Professionnel")
    def get_professionnel(self, obj):
        return obj.professionnel
